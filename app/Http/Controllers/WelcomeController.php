<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Category;
use App\Product;
use App\Customer;
use DB;
use Illuminate\Support\Facades\Session;
class WelcomeController extends Controller
{
    public function index() {
        //$publishedProducts=Product::where('publicationStatus', 1)->get();
        return view('frontEnd.home.homeContent');
    }

   
    public function about(){
        return view('frontEnd.about.about');
    }

    public function createFolder() {
        $user = Customer::find(session::get('customerId'));
        //return $user->id;
        return view('frontEnd.folder.create-folder', ['user'=>$user]);
    }

    public function saveFolder(Request $request) {
        // $this->validate($request, [
        //     'categoryName' => 'required',
        //     'categoryDescription' => 'required',
        // ]);
        //$user = Customer::find(session::get('customerId'));
        //return $request->all();
    //    $category = new Category();
    //    $category->customerId = $request->customerId,
    //    $category->categoryName = $request->categoryName;
    //    $category->categoryDescription = $request->categoryDescription;
    //    $category->publicationStatus = $request->publicationStatus;
    //    $category->save();
    //    return 'Category info save successfully';
    //    Category::create( $request->all() );
    //     return 'Category info save successfully';

        DB::table('categories')->insert([
            'categoryName' => $request->categoryName,
            'customerId' => $request->customerId,
            'categoryDescription' => $request->categoryDescription,
            'publicationStatus' => $request->publicationStatus,
        ]);
        //return redirect()->back();
        return redirect('/folder/show')->with('message', 'Folder save successfully');
    }                                                                                                                                                                                                                                                
    public function manageFolder() {
        $value = session('customerId');
        //$user = Customer::find(session::get('customerId'));
        $categories = Category::where('customerId', $value)
                                //->where('publicationStatus', 1)
                                ->get();
        //return $categories;
        return view('frontEnd.folder.manage-folder', ['categories' => $categories]);
    }

    public function edityFolder($id) {
        $categoryById = Category::where('id', $id)->first();
        return view('frontEnd.folder.edit-folder', ['categoryById' => $categoryById]);
    }
    public function updateFolder(Request $request) {
        //dd( $request->all() );
        $category = Category::find($request->categoryId);
        $category->categoryName = $request->categoryName;
        $category->categoryDescription = $request->categoryDescription;
        $category->publicationStatus = $request->publicationStatus;
        $category->save();
        return redirect('/folder/manage')->with('message', 'Folder info update successfully');
    }
    public function deleteFolder($id) {
        $category = Category::find($id);
        $category->delete();
        return redirect('/folder/manage')->with('message', 'Category info delete successfully');
    }
    public function sowFolder() {
        $value = session('customerId');
        //$user = Customer::find(session::get('customerId'));
        $categories = Category::where('customerId', $value)
                                ->where('publicationStatus', 1)
                                ->get();
        //return $categories;
        return view('frontEnd.folder.show-folder', ['categories' => $categories]);
    }












    public function category($id) {
        $publishedCategoryProducts=Product::where('categoryId', $id)
                                                    ->where('publicationStatus', 1)
                                                    ->get();
        return view('frontEnd.category.categoryContent', ['publishedCategoryProducts'=>$publishedCategoryProducts]);
    }
    public function productDetails($id) {
        $productById=Product::where('id', $id) ->first();
         return view('frontEnd.product.productContent', ['productById'=>$productById]);
    }
    public function contact(){
        return view('frontEnd.contact.contact');
    }
    public function exchange(){
        return view('frontEnd.exchange.exchange');
    }
    
}
















