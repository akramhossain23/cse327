@extends('admin.master')
@section('content')

<hr/>
<h3 class="text-center text-success">{{ Session::get('message') }}</h3>
<hr/>
<h2></h2>
<h3></h3>
<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            
            <th>User Name</th>
            <th>Phone Number</th>
            <th>Address</th>
            <th>Email</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        <?php $i=1; ?>
        @foreach ($users as $user)
        <tr>
            <td>{{ $i++ }}</td>
            
            <td>{{ $user->firstName.' '.$user->lastName }}</td>
            <td>{{ $user->phoneNumber }}</td>
            <td>{{ $user->address }}</td>
            <td>{{ $user->emailAddress }}</td>
            <td>
                <a href="" class="btn btn-success">
                    <span class="glyphicon glyphicon-edit"></span>
                </a>
                <a href="" class="btn btn-danger">
                    <span class="glyphicon glyphicon-trash"></span>
                </a>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<hr/>
<div>
     
</div>
@endsection
