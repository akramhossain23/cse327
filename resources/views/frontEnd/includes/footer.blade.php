<div class="footer">
    <div class="container">
        <div class="col-md-3 footer-left">
            
        </div>
        <div class="col-md-9 footer-right">
            <!--
            <div class="col-sm-6 newsleft">
                <h3>SIGN UP FOR NEWSLETTER !</h3>
            </div>
            <div class="col-sm-6 newsright">
                <form>
                    <input type="text" value="Email" onfocus="this.value = '';" onblur="if (this.value == '') {
                                        this.value = 'Email';
                                    }" required="">
                    <input type="submit" value="Submit">
                </form>
            </div> -->
            <div class="clearfix"></div>
            <div class="sign-grds">
                <div class="col-md-4 sign-gd">
                    <h4>Information</h4>
                    <ul>
                        <li><a href="{{ url('/') }}">Home</a></li>
                        
                        <li><a href="{{url('/contact')}}">Contact</a></li>
                        
                    </ul>
                </div>

                <div class="col-md-4 sign-gd-two">
                    <h4>Store Information</h4>
                    <ul>
                        <li><i class="glyphicon glyphicon-map-marker" aria-hidden="true"></i>Address :  <span>Country Office, 14 Rd 35, Dhaka 1212 .</span></li>
                        <li><i class="glyphicon glyphicon-envelope" aria-hidden="true"></i>Email : <a href="mailto:info@example.com">planbdintl@gov.org</a></li>
                        <li><i class="glyphicon glyphicon-earphone" aria-hidden="true"></i>Phone :01955-328926</li>
                    </ul>
                </div>
                <div class="col-md-4 sign-gd flickr-post">
                    
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
        <div class="clearfix"></div>
        <p class="copy-right">| |</a></p>
    </div>
</div>