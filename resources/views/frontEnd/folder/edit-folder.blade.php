@extends('frontEnd.master')

@section('title','Edit Folder')

@section('mainContent')

<hr/>

<div class="row">
        <div class="col-md-6 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
                <div class="well">
                {!! Form::open( [ 'url'=>'folder/update', 'method' =>'POST', 'class' =>'form-horizontal', 'name'=>'editCategoryForm' ] ) !!}
            <div class="form-group">
                <label for="inputEmail3" class="col-sm-2 control-label">Folder Name</label>
                <div class="col-sm-10">
                    <input type="text" value="{{ $categoryById->categoryName }}" class="form-control" name="categoryName">
                    <input type="hidden" value="{{ $categoryById->id }}" class="form-control" name="categoryId">
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Folder Description</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="categoryDescription" rows="8">{{ $categoryById->categoryDescription }}</textarea>
                </div>
            </div>
            <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Publication Status</label>
                <div class="col-sm-10">
                    <select class="form-control" name="publicationStatus">
                        <option>Select Publication Status</option>
                        <option value="1">Published</option>
                        <option value="0">Unpublished</option> 
                    </select>
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" name="btn" class="btn btn-success btn-block">Update Folder Info</button>
                </div>
            </div>
            {!! Form::close() !!}
            </div>

            </div>
        </div>
</div>




@endsection


