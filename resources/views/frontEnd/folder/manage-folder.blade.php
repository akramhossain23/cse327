@extends('frontEnd.master')

@section('title','Create New Folder')

@section('mainContent')


<div class="row">
        <div class="col-md-6 col-md-offset-1">
        <hr/>
        <h3 class="text-center text-success">{{ Session::get('message') }}</h3>

<table class="table table-bordered table-hover">
    <thead>
        <tr>
            <th>ID</th>
            <th>Folder Name</th>
            <th>Folder Description</th>
            <th>Publication Status</th>
            <th>Action</th>
        </tr>
    </thead>
    <tbody>
        @foreach($categories as $category)
        <tr> 
            <th scope="row">{{$category->id}}</th>
            <td>{{$category->categoryName}}</td>
            <td>{{$category->categoryDescription}}</td>
            <td>{{ $category->publicationStatus == 1 ? 'Published' : 'Unpublished' }}</td> 
            <td>
                <a href="{{ url('/folder/edit/'.$category->id) }}" class="btn btn-success">
                    <span class="glyphicon glyphicon-edit"></span>
                </a>
                <a href="{{ url('/folder/delete/'.$category->id) }}" class="btn btn-danger" onclick="return confirm('Are you sure to delete this'); ">
                    <span class="glyphicon glyphicon-trash"></span>
                </a>
            </td> 
        </tr>
        @endforeach
    </tbody>
</table>
        
        </div>
</div>
<hr/>





@endsection


