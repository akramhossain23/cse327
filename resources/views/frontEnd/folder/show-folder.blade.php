@extends('frontEnd.master')

@section('title','Edit Folder')

@section('mainContent')

<hr/>
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<div class="row">
        <div class="col-md-6 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">
                <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
                <div class="well">
                @foreach($categories as $category)
                
                <button style="font-size:24px">{{ $category->categoryName }} <i class="fa fa-folder" style="font-size:48px;color:red"></i></button>
                @endforeach
            </div>
                
            </div>
        </div>
</div>




@endsection


