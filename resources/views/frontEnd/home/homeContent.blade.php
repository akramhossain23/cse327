@extends('frontEnd.master')

@section('title')
Home
@endsection
@section('mainContent')
<div class="banner-grid">
    <div id="visual">
        
        <div class="clearfix"></div>
    </div>
    <script type="text/javascript" src="{{ asset('frontEnd/js/pignose.layerslider.js') }}"></script>
    <script type="text/javascript">
//<![CDATA[
$(window).load(function () {
    $('#visual').pignoseLayerSlider({
        play: '.btn-play',
        pause: '.btn-pause',
        next: '.btn-next',
        prev: '.btn-prev'
    });
});
//]]>
    </script>

</div>
<!-- //banner -->
<!-- content -->

<!-- //content -->

<!-- content-bottom -->


<!-- //content-bottom -->
<!-- product-nav -->

<div class="product-easy">
    <div class="container">


        <div class="sap_tabs">
            <div id="horizontalTab" style="display: block; width: 100%; margin: 0px;">
                				  	 
                <div class="resp-tabs-container">
                    
                <img src="{{ asset('cover/') }}/cover.png" alt="Cover Image" width="1200" height="400">
                    	
                </div>	
            </div>
        </div>
    </div>
</div>

<script src="{{ asset('frontEnd/js/easyResponsiveTabs.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $('#horizontalTab').easyResponsiveTabs({
            type: 'default', //Types: default, vertical, accordion
            width: 'auto', //auto or any width like 600px
            fit: true   // 100% fit in a container
        });
    });

</script>
@endsection


