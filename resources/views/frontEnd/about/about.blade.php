@extends('frontEnd.master')

@section('title')
About
@endsection

@section('mainContent')
<div class="container">


<div class="row" style="margin-top: 50px">

    <div class="col-lg-2">

    </div>

    <div class="col-sm-12 col-lg-8">
        <div class="panel panel-info">
        <p>Plan International began working in Bangladesh in 1994. Across all our work, we encourage children and young people to be leaders in their communities and drive progress on the issues that matter to them. 
        We also integrate gender equality and inclusion across all our areas of work.</p>
        <br>
        <p>Among the issues affecting children’s development in Bangladesh are a lack of skilled birth attendants, malnutrition, a lack of sexual health rights, low quality education, a high number of girls dropping out of school, a lack of safety,
         especially for girls, low birth registration rates and poor hygiene practices such as open defecation. In addition, Bangladesh is one of the most disaster-prone countries in the world with the most vulnerable children living in the worst-affected areas.</p>
         <br>
         <p>Plan International Bangladesh targets the most excluded children and our programmes directly benefit over 2.3 million children across the country.</p>

        </div>

    </div>

</div>


</div>


@endsection
