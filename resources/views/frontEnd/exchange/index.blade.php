@extends('frontEnd.master')

@section('title','Exchange books')

@section('mainContent')

    <div class="container">

        <div class="row " style="margin-top: 50px">

            <div class="col-lg-3">

            </div>

            <div class="col-lg-6">

                <table class="table" width="500">


                    <tr >
                        <td><a href="{{ route('book.profile') }}" class="btn btn-info btn-block btn-lg">User Info</a></td>

                    </tr>
                    <tr >
                        <td><a href="{{ route('create.folder') }}" class="btn btn-info btn-block btn-lg">Folder Create</a></td>

                    </tr>
                    <tr >
                        <td><a href="{{ route('manage.folder') }}" class="btn btn-info btn-block btn-lg">Manage Folder</a></td>

                    </tr>
                    <tr >
                        <td><a href="{{ route('show.folder') }}" class="btn btn-info btn-block btn-lg">Show Folder</a></td>

                    </tr>
                    

                </table>

            </div>

        </div>

    </div>


@endsection


