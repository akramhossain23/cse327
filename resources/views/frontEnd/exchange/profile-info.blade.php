@extends('frontEnd.master')

@section('title','Profile Info')

@section('mainContent')

<div class="row">
        <div class="col-md-6 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">

                    <div class="panel-body">
                        <h3 class="text-center text-success">Hello  ,</h3>
                        <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
                        <table class="table table-borderd" >
                            <tr class="bg-success">
                                <th>ID No</th>
                                <td></td>
                            </tr>
                            <tr class="bg-success">
                                <th>Full Name</th>
                                <td></td>
                            </tr>
                            <tr class="bg-success">
                                <th>Phone Number</th>
                                <td></td>
                            </tr>
                            <tr class="bg-success">
                                <th>Email Address</th>
                                <td></td>
                            </tr>
                            <tr class="bg-success">
                                <th></th>
                                <td>
                                    <a href="" class="btn btn-success btn-xs" title="Edit User Info">
                                        <span class="glyphicon glyphicon-edit" ></span>
                                    </a>
                                   
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection


