@extends('frontEnd.master')

@section('title','Edit Profile')

@section('mainContent')

<div class="row">
        <div class="col-md-6 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">

                </div>
                <div class="panel-body">
                    <h3 class="text-center text-success">{{ Session::get('message') }}</h3>
                    {!! Form::open([ 'route'=>'update-user-info','method'=>'POST' ]) !!}
                        
                        <div class="form-group">
                            <label class="control-label col-md-4">First Name</label>
                            <div class="col-md-6">
                                <input type="text" name="firstName" class="form-control" value="{{ $user->firstName }}">

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Last Name</label>
                            <div class="col-md-6">
                                <input type="text" name="lastName" class="form-control" value="{{ $user->lastName }}">

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Phone Number</label>
                            <div class="col-md-6">
                                <input type="text" name="phoneNumber" class="form-control" value="{{ $user->phoneNumber }}">

                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-4">Email Address</label>
                            <div class="col-md-6">
                                <input type="text" name="emailAddress" class="form-control" value="{{ $user->emailAddress }}">

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-4">Address</label>
                            <div class="col-md-6">
                                <input type="text" name="address" class="form-control" value="{{ $user->address }}">

                            </div>
                        </div>
                        <div class="form-group">

                            <div class="col-md-6">

                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-">
                                <input type="submit" name="btn" class="btn btn-success btn-block" value="Update User Info">
                            </div>
                        </div>

                    {!! Form::close() !!}
                </div>
            </div>
        </div>
    </div>


@endsection


